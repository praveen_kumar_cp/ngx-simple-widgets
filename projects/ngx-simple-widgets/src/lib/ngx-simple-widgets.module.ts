import { NgModule } from '@angular/core';
import { NgxSimpleWidgetsComponent } from './ngx-simple-widgets.component';



@NgModule({
  declarations: [NgxSimpleWidgetsComponent],
  imports: [
  ],
  exports: [NgxSimpleWidgetsComponent]
})
export class NgxSimpleWidgetsModule { }
