/*
 * Public API Surface of ngx-simple-widgets
 */

export * from './lib/ngx-simple-widgets.service';
export * from './lib/ngx-simple-widgets.component';
export * from './lib/ngx-simple-widgets.module';
